package conversor;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConversorTemperaturaTest {
	
	private ConversorTemperatura meuConversor;
	
	@Before
	public void setUp()throws Exception{
		meuConversor = new ConversorTemperatura();
	}
	
	@Test
	public void testeCelsiusParaFahrenheitComValorDouble(){
		assertEquals (104.0, meuConversor.celsiusParaFahrenheit (40.0),0.01);
	}
	
	@Test
	public void testFahrenheitParaCelsiusComValorFahrenheitDouble() {
		assertEquals(40.0, meuConversor.fahrenheitParaCelsius(104.0), 0.01);
		
	}
	
	@Test
	public void testCelsiusParaKelvinComValorCelsiusDouble() {
		assertEquals(273 , meuConversor.celsiusParaKelvin(0), 0.01);
		
	}
	
	@Test
	public void testKelvinParaCelsiusComValorKelvinDouble() {
		assertEquals(0 , meuConversor.kelvinParaCelsius(273), 0.01);
		
	}
	
	
	@Test
	public void testFahrenheitParaKelvinComValorFahrenheitDouble() {
		assertEquals(373 , meuConversor.FahrenheitParaKelvin(212), 0.01);
		
	}
	
	@Test
	public void testKelvinParaFahrenheitComValorKelvinDouble() {
		assertEquals(212 , meuConversor.kelvinParaFahrenheit(373), 0.01);
		
	}	


}